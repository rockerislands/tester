﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Windows.Forms;

namespace TestEditor
{
    internal class Encr
    {
        static byte[] Key = { 0xBF, 0x21, 0xAB, 0x64, 0xDE, 0x64, 0x48, 0xF1, 0x85, 0xF2, 0x82, 0xD9, 0xE9, 0xC9, 0x78, 0x13, 0x0, 0xAA, 0x7F, 0x24, 0xCA, 0xB1, 0xB6, 0xA5, 0xD5, 0x3E, 0x90, 0xB7, 0xC1, 0x18, 0x1B, 0x59 };
        static byte[] IV = { 0xBE, 0x35, 0x47, 0xAB, 0x6C, 0xD0, 0x16, 0xCE, 0x84, 0x31, 0x0, 0x74, 0xA7, 0x1D, 0x7D, 0xA9 };

        static public string EncryptStr(string a)
        {
            using (Rijndael rnl = Rijndael.Create())
            {
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rnl.CreateEncryptor(Key, IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        byte[] bbb = Encoding.UTF8.GetBytes(a);
                        csEncrypt.Write(bbb, 0, bbb.Length);
                        csEncrypt.FlushFinalBlock();
                        return Convert.ToBase64String(msEncrypt.ToArray());
                        
                    }
                    
                }
            }
        }
        static public string DecryptStr(string b)
        {
            Rijndael rnl = Rijndael.Create();
            ICryptoTransform decryptor = rnl.CreateDecryptor(Key, IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Write))
                {
                    byte[] a = Convert.FromBase64CharArray(b.ToCharArray(), 0, b.Length);
                    csEncrypt.Write(a, 0, a.Length);
                    csEncrypt.FlushFinalBlock();
                    return Encoding.UTF8.GetString(msEncrypt.ToArray());

                }

            }
        }
        /*static public string PrintByteArray(byte[] bytes)
        {
            var sb = new StringBuilder("{ ");
            foreach (var b in bytes)
            {
                sb.Append("0x" + b.ToString("X") + ", ");
            }
            sb.Append("}");
            return sb.ToString();
        }*/
    }
}
