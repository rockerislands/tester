﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace TestEditor
{
    public partial class Form1 : Form
    {
        List<Question> qList = new List<Question>();
        public Form1()
        {
            InitializeComponent();
        }
        void ShowQuestion(int id)
        {
            if (id < 0 || id > qList.Count) return; //
            textBox2.Text = qList[id].name;
            textBox1.Text = qList[id].text;
            textBox3.Text = qList[id].answ[0].text;
            textBox4.Text = qList[id].answ[1].text;
            textBox5.Text = qList[id].answ[2].text;
            textBox6.Text = qList[id].answ[3].text;
            radioButton1.Checked = qList[id].answ[0].right;
            radioButton2.Checked = qList[id].answ[1].right;
            radioButton3.Checked = qList[id].answ[2].right;
            radioButton4.Checked = qList[id].answ[3].right;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string name = "Вопрос " + (listBox1.Items.Count + 1).ToString();
            if (qList.Count == 0)
            {
                EnableGUI();
            }
            qList.Add(new Question(name));
            listBox1.Items.Add(name);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            ShowQuestion(listBox1.SelectedIndex);
            

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowQuestion(listBox1.SelectedIndex);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex !=-1)
            {
                qList[listBox1.SelectedIndex].name = textBox2.Text;
                listBox1.Items[listBox1.SelectedIndex] = textBox2.TextLength > 28 ? textBox2.Text.Substring(0, 28) + "..." : textBox2.Text;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].text = textBox1.Text;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = listBox1.SelectedIndex;
            if (id > 0)
            {
                qList.Insert(id - 1, qList[id]);
                listBox1.Items.Insert(id - 1, listBox1.Items[id]);

                qList.RemoveAt(id + 1);
                listBox1.Items.RemoveAt(id + 1);
                listBox1.SelectedIndex = id - 1;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int id = listBox1.SelectedIndex;
            if (id < listBox1.Items.Count - 1)
            {
                qList.Insert(id + 2, qList[id]);
                listBox1.Items.Insert(id + 2, listBox1.Items[id]);

                qList.RemoveAt(id);
                listBox1.Items.RemoveAt(id);
                listBox1.SelectedIndex = id + 1;
            }
        }
        void EnableGUI()
        {
            textBox2.Enabled =
                    textBox1.Enabled =
                    button1.Enabled =
                    button3.Enabled =
                    button4.Enabled =
                    radioButton1.Enabled =
                    radioButton2.Enabled =
                    radioButton3.Enabled =
                    radioButton4.Enabled =
                    menuItem3.Enabled = //сохранить
                    menuItem4.Enabled = //закрыть
                    textBox3.Enabled =
                    textBox4.Enabled =
                    textBox5.Enabled =
                    textBox6.Enabled = true;
        }
        void DisableGUI()
        {
            textBox2.Enabled =
                    textBox1.Enabled =
                    button1.Enabled =
                    button3.Enabled =
                    button4.Enabled =
                    radioButton1.Enabled =
                    radioButton2.Enabled =
                    radioButton3.Enabled =
                    radioButton4.Enabled =
                    textBox3.Enabled =
                    textBox4.Enabled =
                    textBox5.Enabled =
                    textBox6.Enabled =
                    menuItem3.Enabled = //сохранить
                    menuItem4.Enabled = //закрыть
                    radioButton1.Checked =
                    radioButton2.Checked =
                    radioButton3.Checked =
                    radioButton4.Checked =
                    false;
            textBox1.Text =
                textBox2.Text =
                textBox3.Text =
                textBox4.Text =
                textBox5.Text =
                textBox6.Text = "";

        }
        private void button3_Click(object sender, EventArgs e)
        {
            int id = listBox1.SelectedIndex;
            if (id == listBox1.Items.Count - 1) id--;
            qList.RemoveAt(listBox1.SelectedIndex);
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            if (listBox1.Items.Count > 0)
            {
                listBox1.SelectedIndex = id;
                ShowQuestion(id);
            }
            else
            {
                DisableGUI();
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[0].text = textBox3.Text;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[1].text = textBox4.Text;
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[2].text = textBox5.Text;
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[3].text = textBox6.Text;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[0].right = radioButton1.Checked;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[1].right = radioButton2.Checked;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[2].right = radioButton3.Checked;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                qList[listBox1.SelectedIndex].answ[3].right = radioButton4.Checked;
            }
        }

        private void menuItem6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog2.ShowDialog(); //СДЕЛАТЬ ШИФРОВАНИЕ!!!!!!!!!!!!!!!!!!!!!!!!!!1111111111111111111111
            if (result == DialogResult.OK)
            {
                if (qList.Count > 0)
                {
                    string ab = "<tester>";

                    foreach (Question a in qList)
                    {
                        ab+="<question>";
                        ab+="<name>" + a.name + "</name>";
                        ab+="<text>" + a.text + "</text>";
                        ab+="<answer>" + a.answ[0].text + (a.answ[0].right ? "<right/>" : "") + "</answer>";
                        ab+="<answer>" + a.answ[1].text + (a.answ[1].right ? "<right/>" : "") + "</answer>";
                        ab+="<answer>" + a.answ[2].text + (a.answ[2].right ? "<right/>" : "") + "</answer>";
                        ab+="<answer>" + a.answ[3].text + (a.answ[3].right ? "<right/>" : "") + "</answer>";
                        ab += "</question>";
                    }
                    ab += "</tester>";
                    /*StreamWriter writer = new StreamWriter(saveFileDialog2.FileName);
                    writer.WriteLine("<tester>");
                    foreach (Question a in qList)
                    {
                        writer.WriteLine("<question>");
                        writer.WriteLine("<name>" + a.name + "</name>");
                        writer.WriteLine("<text>" + a.text + "</text>");
                        writer.WriteLine("<answer>" + a.answ[0].text + (a.answ[0].right ? "<right/>" : "") + "</answer>");
                        writer.WriteLine("<answer>" + a.answ[1].text + (a.answ[1].right ? "<right/>" : "") + "</answer>");
                        writer.WriteLine("<answer>" + a.answ[2].text + (a.answ[2].right ? "<right/>" : "") + "</answer>");
                        writer.WriteLine("<answer>" + a.answ[3].text + (a.answ[3].right ? "<right/>" : "") + "</answer>");
                        writer.WriteLine("</question>");
                    }
                    writer.WriteLine("</tester>");
                    writer.Close();
                     */
                    //MessageBox.Show(ab);
                    StreamWriter writer = new StreamWriter(saveFileDialog2.FileName);
                    string aaa = Encr.EncryptStr(ab);
                    writer.Write(aaa);
                    writer.Close();
                }
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog2.ShowDialog();
            if (result == DialogResult.OK)
            {
                StreamReader sreader = new StreamReader(openFileDialog2.FileName);
                string aaa = sreader.ReadToEnd();
                
                string res = Encr.DecryptStr(aaa); 
                //РАСШИФРОВАТЬ ТУТ
                


                qList.Clear();
                listBox1.Items.Clear();

                int flag = -1;
                int lastid = -1, anwid = 0;
                try
                {
                    using (XmlReader reader = XmlReader.Create(new StringReader(res)))
                    {
                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    switch (reader.Name)
                                    {
                                        case "tester":
                                            flag = 0;
                                            break;
                                        case "question":
                                            if (flag == 0)
                                            {
                                                flag = 1;
                                                qList.Add(new Question());
                                                lastid = qList.Count - 1;
                                                anwid = 0;
                                            }
                                            break;
                                        case "name": if (flag == 1) flag = 2; break;
                                        case "text": if (flag == 1) flag = 3; break;
                                        case "answer": if (flag == 1) flag = 4; break;
                                        case "right":
                                            if (flag == 4)
                                            {
                                                qList[lastid].answ[anwid].right = true;
                                            }
                                            break;
                                    }
                                    break;
                                case XmlNodeType.Text:
                                    switch (flag)
                                    {
                                        case 2:
                                            qList[lastid].name = reader.Value;
                                            listBox1.Items.Add(reader.Value);
                                            break;
                                        case 3:
                                            qList[lastid].text = reader.Value;
                                            break;
                                        case 4:
                                            qList[lastid].answ[anwid].text = reader.Value;
                                            break;
                                    }
                                    break;
                                case XmlNodeType.EndElement:
                                    switch (flag)
                                    {
                                        case 1: flag = 0; break;
                                        case 2:
                                        case 3: flag = 1; break;
                                        case 4:
                                            flag = 1;
                                            anwid++;
                                            break;
                                    }
                                    break;
                            }

                        }
                    }
                }
                catch(Exception ee) { MessageBox.Show(ee.ToString()); }
                sreader.Close();
                if (qList.Count > 0)
                {
                    listBox1.SelectedIndex = listBox1.Items.Count - 1;
                    ShowQuestion(listBox1.SelectedIndex);
                    EnableGUI();
                }
                else DisableGUI();
            }
        }


        private void menuItem4_Click(object sender, EventArgs e)
        {
            qList.Clear();
            listBox1.Items.Clear();
            DisableGUI();
        }


    }
    internal class Answer
    {
        public string text;
        public bool right;
        public Answer(string Text)
        {
            text = Text;
        }
        public Answer() { }
    }
    internal class Question
    {
        public string name;
        public string text;
        public Answer[] answ = new Answer[4];
        public Question()
        {
            answ[0] = new Answer();
            answ[1] = new Answer();
            answ[2] = new Answer();
            answ[3] = new Answer();
        }
        public Question(string Name)
        {
            name = Name;
            answ[0] = new Answer("Ответ 1");
            answ[1] = new Answer("Ответ 2");
            answ[2] = new Answer("Ответ 3");
            answ[3] = new Answer("Ответ 4");
            answ[0].right = true;
        }
    }
    
}
