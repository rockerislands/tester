﻿//using System;
//using System.Windows.Forms;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;

namespace Tester
{
    //public partial class Form1 : Form
    //{
    //    List<Question> qList = new List<Question>();
    //}
    class Answer
    {
        public string text;
        public bool right;
        public Answer(string Text)
        {
            text = Text;
        }
        public Answer() { }
    }
    class Question
    {
        public string name;
        public string text;
        public Answer[] answ = new Answer[4];
        public Question()
        {
            answ[0] = new Answer();
            answ[1] = new Answer();
            answ[2] = new Answer();
            answ[3] = new Answer();
        }
        public Question(string Name)
        {
            name = Name;
            answ[0] = new Answer("Ответ 1");
            answ[1] = new Answer("Ответ 2");
            answ[2] = new Answer("Ответ 3");
            answ[3] = new Answer("Ответ 4");
            answ[0].right = true;
        }
    }
}
