﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;


namespace Tester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        resultsigner result = new resultsigner();

        List<Question> qList = new List<Question>();

        sbyte[] answrs = new sbyte[100];
        

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void загрузитьТестToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                StreamReader sreader = new StreamReader(openFileDialog1.FileName);
                string aaa = sreader.ReadToEnd();
                string res = Encr.DecryptStr(aaa); 
                //РАСШИФРОВАТЬ ТУТ
                qList.Clear();
                listBox1.Items.Clear();

                int flag = -1;
                int lastid = -1, anwid = 0;
                try
                {
                    using (XmlReader reader = XmlReader.Create(new StringReader(res)))
                    {
                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    switch (reader.Name)
                                    {
                                        case "tester":
                                            flag = 0;
                                            break;
                                        case "question":
                                            if (flag == 0)
                                            {
                                                flag = 1;
                                                qList.Add(new Question());
                                                lastid = qList.Count - 1;
                                                anwid = 0;
                                            }
                                            break;
                                        case "name": if (flag == 1) flag = 2; break;
                                        case "text": if (flag == 1) flag = 3; break;
                                        case "answer": if (flag == 1) flag = 4; break;
                                        case "right":
                                            if (flag == 4)
                                            {
                                                qList[lastid].answ[anwid].right = true;
                                            }
                                            break;
                                    }
                                    break;
                                case XmlNodeType.Text:
                                    switch (flag)
                                    {
                                        case 2:
                                            qList[lastid].name = reader.Value;
                                            listBox1.Items.Add(reader.Value);
                                            break;
                                        case 3:
                                            qList[lastid].text = reader.Value;
                                            break;
                                        case 4:
                                            qList[lastid].answ[anwid].text = reader.Value;
                                            break;
                                    }
                                    break;
                                case XmlNodeType.EndElement:
                                    switch (flag)
                                    {
                                        case 1: flag = 0; break;
                                        case 2:
                                        case 3: flag = 1; break;
                                        case 4:
                                            flag = 1;
                                            anwid++;
                                            break;
                                    }
                                    break;
                            }

                        }
                    }
                }
                catch (Exception ee) { MessageBox.Show(ee.ToString()); }
                if (qList.Count > 0)
                {
                    sbyte[] answrs = new sbyte[qList.Count];
                    listBox1.SelectedIndex = 0;//listBox1.Items.Count - 1;
                    ShowQuestion(listBox1.SelectedIndex);
                    EnableGUI();
                }
                else DisableGUI();

               
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowQuestion(listBox1.SelectedIndex);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > 0)
            {
                listBox1.SelectedIndex--;
                //ShowQuestion(listBox1.SelectedIndex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < listBox1.Items.Count - 1)
            {
                listBox1.SelectedIndex++;
                //ShowQuestion(listBox1.SelectedIndex);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < listBox1.Items.Count - 1)
            {
                listBox1.SelectedIndex++;
                //ShowQuestion(listBox1.SelectedIndex);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton1.Checked && listBox1.SelectedIndex != -1)
            {
                answrs[listBox1.SelectedIndex] = 1;
            }

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked && listBox1.SelectedIndex != -1)
            {
                answrs[listBox1.SelectedIndex] = 2;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked && listBox1.SelectedIndex != -1)
            {
                answrs[listBox1.SelectedIndex] = 3;
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked && listBox1.SelectedIndex != -1)
            {
                answrs[listBox1.SelectedIndex] = 4;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //DisableGUI();

            int counter=0;
            for(int i =0 ; i< qList.Count;i++)
            {
                if (answrs[i]!=0 && qList[i].answ[answrs[i]-1].right) counter++;
            }

            //MessageBox.Show("У вас "+counter.ToString()+" правильных ответов из "+ qList.Count.ToString()+ " вопросов","Проверка!");

            result.Show();
        }

    }

}

