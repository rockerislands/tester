﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tester
{
    public partial class Form1 : Form
    {
        void EnableGUI()
        {
            button1.Enabled =
            button3.Enabled =
            button4.Enabled =
            button2.Enabled =
            radioButton1.Enabled =
            radioButton2.Enabled =
            radioButton3.Enabled =
            radioButton4.Enabled =
            listBox1.Enabled = true;
        }
        void DisableGUI()
        {
            button1.Enabled =
            button2.Enabled =
            button3.Enabled =
            button4.Enabled =
            radioButton1.Enabled =
            radioButton2.Enabled =
            radioButton3.Enabled =
            radioButton4.Enabled =
            listBox1.Enabled = false;

            radioButton1.Checked =
                radioButton2.Checked =
                radioButton3.Checked =
                radioButton4.Checked = false;

            listBox1.SelectedIndex = -1;
        }
        void ShowQuestion(int id)
        {
            if (id < 0 || id > qList.Count) return; //

            switch (answrs[listBox1.SelectedIndex])
            {
                case 1: radioButton1.Checked = true;
                    break;
                case 2: radioButton2.Checked = true;
                    break;
                case 3: radioButton3.Checked = true;
                    break;
                case 4: radioButton4.Checked = true;
                    break;
                case 0:
                        radioButton1.Checked =
                        radioButton2.Checked =
                        radioButton3.Checked =
                        radioButton4.Checked = false;
                    break;
            }

            label1.Text = qList[id].name;
            label1.Text += "\n" + qList[id].text;

            radioButton1.Text = qList[id].answ[0].text;
            radioButton2.Text = qList[id].answ[1].text;
            radioButton3.Text = qList[id].answ[2].text;
            radioButton4.Text = qList[id].answ[3].text;



        }

    }
}
